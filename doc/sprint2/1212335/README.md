RCOMP 2021-2022 Project - Sprint 2 - Member 1212335 folder
===========================================


## IP Adress Block Used for Building 4 
* 172.18.149.0/24
* Usable Min Ip: 172.18.149.1
* Usable Max Ip: 172.18.149.254  
* Broadcast: 	172.18.149.255
* Total Usable Hosts: 254

## IPv4 Address Distribution

| Node    | Prefix      |    IP   |
| ----------- | ----------- |----------- |
| WIFI: 70   |  /25	       | 172.18.149.0       |
|  First Floor: 55    |  /26		    |   172.18.149.128       |
| Ground Floor: 28   |  /27       |  172.18.149.192       |
| IP-phones: 12 |  /28	       | 172.18.149.225           |
| Local Servers/DMZ: 10  |  /28	|172.18.149.240      |




## Vlan IDs ##

Building Range:  486-490

|  Nodes | Vlan Id    | Name     
|--------| -----------|-----------
|Ground Floor| 486   |   b4ground 	 |
|First Floor|  487  | 	b4floor1   |
| WIFI| 488   |   b4wifi 	 |
|IP-phones| 489   |   b4voip	 |
| Local Servers/DMZ| 490   | b4dmz  	 |

## Other Valuable Information

Please see the new Sprint1 in the sprint1 folder. I didn't really know where we were supposed to upload the new versions but with how bad they were I don't think you'd mind us deleting our new ones.
For some reason, my laptop connected to the Access Point refuses to retain its IP after a software reboot. I don't know if this is something Cisco does but I'm mentioning it here as I sent you a message on Teams, with no response. I usually set the laptop on 172.18.149.2/25.
Also, I always have to ping computers once before succeeding. The first try will always result in failure, the next tries are always successful.