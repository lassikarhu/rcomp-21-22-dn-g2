## Building 5 ###
### IP distribution table


| Nodes | Prefix | IP |
| --------------- | ----------- | --------------- |
| WIFI   | /26 | 172.18.150.0 |
| Floor1 | /26 | 172.18.150.64 |
| Ground | /26 | 172.18.150.128 |
| VOIP   | /27 | 172.18.150.192 |
| DMZ    | /27 | 172.18.150.224|

### Vlan ID table

|        Nodes        |   VLAN ID   |     Name      |
| ------------------- | ----------- | ------------- |
| Ground  | 491 | b5ground |
| Floor 1 | 492 | b5floor1 |
| WIFI    | 493 | b5wifi   |
| VOIP    | 494 | b5voip   |
| DMZ     | 495 | b5dmz    | 