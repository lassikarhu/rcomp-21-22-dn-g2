Planning 

  

First building - 1212308 

Second building - 1212343 

Third building - 1212273 

Fourth building - 1212335

Fifth building - 1212349

  

  

The first thing we did was to select the correct VLANS to the proper buildings. Theo had the job of putting them clearly in a photo so we could implement them effortlessly. Then, we summed up all the nodes in each building in order to do the subnetting.  

We have implemented each building network using the requested equipment: PT-empty Switches, Copper or fiber cables, model 7960 IP-phones, 2811 Router, and PC’s. 

Usually after a building is done, we would try to connect it to the main building to see if it properly connects. If it did not connect, we would focus on that building alone, each student testing different solutions. 

Once everybody finished, we all met up to put everything in place, just like a puzzle.  

 

The Cisco Packet Tracer version we all used is 8.1.1.0022. 

 

The difficulties we encountered were: 

 Not understanding in the first place what to do 

Sometimes the packet tracer would not save the IP address for the PC, it was hard to pinpoint why the pings were not working at some point. 

 