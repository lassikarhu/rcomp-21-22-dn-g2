![Building1](C:\Users\lassi\Pictures\Building1.png)

# Building 1

Floor 0 most of the computers are connected to WiFi. the computers that are not connected on wifi are connected to room 1.0.8 Switch with copper Cat6 cables. The maximum cable length for that is 30m. The two access points for floor 0 covers the whole room with good connection in every corner. The switches are connected to each other using STP-protocol and fiber cables providing fast and redundant connection.

Floor 1 most of the computers are also connected to wifi. The computers that are not connected to wifi are connected to room 1.1.7 switch witth copper Cat6 cables. The maximum cable length for that is 70m. The two access points covers the whole room and provides good connection to every corner. The switches are connected to each oher using STP-protocol and fiber cables. The room 1.1.1 switches are also connected to room 1.0.2 main connect using STP and fiber cables.

The number of every rooms endpoints have been decided using the rule 2endpoints per 10m^2.

## Room areas

### Floor 0

* Room 1.0.1 - 20m
* Room 1.0.2 - 30m
* Room 1.0.3 - 30m
* Room 1.0.4 - 45m
* Room 1.0.5 - 15m
* Room 1.0.6 - 15m
* Room 1.0.7 - 15m
* Room 1.0.8 - 30m
* Room 1.0.9 - 27m
* Room 1.0.10 - 42m

### Floor 1

* Room 1.1.1 - 20m
* Room 1.1.2 - 50m
* Room 1.1.3 - 55m
* Room 1.1.4 - 14m
* Room 1.1.5 - 14m
* Room 1.1.6 - 14m
* Room 1.1.7 - 14m
* Room 1.1.8 - 14m
* Room 1.1.9 - 14m
* Room 1.1.10 - 14m
* Room 1.1.11 - 14m
* Room 1.1.12 - 14m
* Room 1.1.13 - 14m
* Room 1.1.14 - 42m

## Backbone cable lengths

### Floor 0

Two 30m Fiber cables for room 1.0.8 switch to room 1.0.8 switches and one 2m fiber cable connecting room 1.0.2 switches.

### Floor 1

Two 28m fiber cables for room 1.1.7 switch to room 1.1.1 switches and one 3m fiber cable connecting room 1.1.1 switches

### Building

Two 15m fiber cables connecting room 1.1.1 switches to room 1.0.2 main connect switch.