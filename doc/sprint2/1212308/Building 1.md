# Building 1

### Vlans:

* Backbone = 470
* Floor 0 = 471
* Floor 1 = 472
* Wifi = 473
* VoIP = 474
* DMZ = 475

### Address Ranges:

* Backbone = 172.18.144.0/25 - 172.18.144.128/25
* DMZ = 172.18.144.129/25 - 172.18.144.256/25
* WiFi = 172.18.145.0/25 - 172.18.144.128/25
* Floor 0 = 172.18.145.129/26 - 172.18.145.192/26
* VoIP = 172.18.145.193/26 - 172.18.145.256/26
* Floor 1 = 172.18.146.0/25 - 172.18.146.128/25

## Gateways:

* Floor 0 = 172.18.145.130
* WiFi = 172.18.145.1
* backbone = 172.18.144.1
* VoIP = 172.18.145.194
* Floor 1 = 172.18.146.1
* DMZ = 172.18.144.130

I used fiber connections and redundant links between the switches.





