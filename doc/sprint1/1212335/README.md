## Beginnings

With how poorly our Sprint1 went, I spent a lot more time reading the project description and did my best to review my past work in introspective.
I've started by establishing a way to use the scale, as at first I found it really confusing. I ended up using the ration pixel/meter to work my way through. (some measurements in pixels are seen on the first Document)
My first (real) step was to scale up all room measurements and figure out the number of available outlets per room. Once that was done, I decided, pretty randomly, to place the Access Point in room 4.0.2 rather than in 4.0.5 like in my past Sprint1. This decision was made because it felt better to place said device in a room that would be closer to the middle of the overall building.

## Ground & First floor

Once the number of outlets figured out, it was only a matter of placing them accordingly to the 3 meter rule; every person in a room should be maximum 3 meters away from an outlet. This condition was quite hard to respect as I have not figured out a simple way of making sure of my every outlet placement. I ended up only checking this condition in big rooms with a high outlet density. Hence why you will find outlets in the middle of certain rooms like 4.0.4 or 4.1.1. I decided I would connected all my Consolidation Points together and to the Horizontal Cross Connects using fiber cables, as the required length doesn't exceed the maximum fiber cable length that was implied. Moreover, all devices are connected to each other redundantly (Horizontal Cross Connects togethers, Consolidation Points together). This prevents full system breakdown in case of a cable failure.
