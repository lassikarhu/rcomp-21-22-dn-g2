# Planning
1. First building - 1212308
2. Second building - 1212343
3. Third building - 1212273
4. Fourth building - 1212335
5. fifth building - 1212349

## Technical rules

Main connectors//backbones:
•	Main Cross Connector: brain of the network, everything is connected there
•	Intermediate Cross Connector: one per building -> connected to the MCC and connecting the HCC
•	Horizontal Cross Connector: one per floor -> connected to ICCs, provides cable connections to users
•	Consolidation Points: might be appropriate for high outlet density

Cable Types:
•	Optical fiber: super fast and fragile
•	Copper Cable: NEVER longer than 90 meters, up to 10 Gbps

Horizontal Cabling Rules:
•	The total area covered by a horizontal cross-connect should be less than 1000 m2
•	Each cable (whatever type) length should be less than 90 meters
•	Straight line distance between the horizontal cross-connect and the outlet should be less than 80 meters.
•	Cables connecting an intermediate cross-connect (IC) to a horizontal crossconnect (HC) are limited to 500 meters long
•	Cables connecting the main cross-connect (MC) to an IC are limited to 1500 meters long.

Random rules:
•	2 outlets per 10m² -> for a work area between 10 m2 and 20 m2 , four outlets should be available and for an area between 20 m2 and 30 m2 , six outlets should be available.
•	Wherever the user equipment is, there should always be an outlet less than three meters away.
•	Each access-point device will grant approximately a 50 meters diameter circle coverage.

