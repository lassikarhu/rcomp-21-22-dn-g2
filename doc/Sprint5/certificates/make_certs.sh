#!/bin/bash
STOREPASS="forgotten"
for ENT in certificates/server_J  certificates/client1_J certificates/client2_J certificates/client3_J certificates/client4_J certificates/client5_J certificates/client6_J certificates/client7_J certificates/client8_J; do
 rm -f ${ENT}.jks ${ENT}.pem
 echo -e "${ENT}\nDEI\nISEP\nPORTO\nPORTO\nPT\nyes" | keytool -genkey -v -alias ${ENT} -keyalg RSA -keysize 2048 \
	-validity 365 -keystore ${ENT}.jks -storepass ${STOREPASS}
 keytool -exportcert -alias ${ENT} -keystore ${ENT}.jks -storepass ${STOREPASS} -rfc -file ${ENT}.pem
done
####
echo "Creating trust relations"
for ENT in certificates/client1_J certificates/client2_J certificates/client3_J certificates/client4_J certificates/client5_J certificates/client6_J certificates/client7_J certificates/client8_J; do
 echo "yes"|keytool -import -alias ${ENT} -keystore certificates/server_J.jks -file ${ENT}.pem -storepass ${STOREPASS}
 echo "yes"|keytool -import -alias certificates/server_J -keystore ${ENT}.jks -file certificates/server_J.pem -storepass ${STOREPASS}
done
echo "yes"|keytool -import -alias certificates/server_J -keystore client4_J.jks -file certificates/server_J.pem -storepass ${STOREPASS}
echo "############################################################################"
keytool -list -keystore certificates/server_J.jks -storepass ${STOREPASS}
#######
