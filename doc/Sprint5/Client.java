import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Client {
    private static final Scanner scanner = new Scanner(System.in);


    public Client(){}

    /**
     * Displays all files and fetches the name of the requested file by the user
     * @param transfer
     * @return
     */
    private static String getFileName(ByteTransfer transfer) {
        byte[] spomsp = transfer.ReceiveBytes();
        List<String> files = new ArrayList<>();
        while(spomsp[1] != 5)
        {
            String name = new String(Arrays.copyOfRange(spomsp,4,spomsp.length));
            files.add(name);
            spomsp = transfer.ReceiveBytes();
        }
        System.out.println("Please select a file from the following list");
        files.forEach((f) -> System.out.println(f));
        String filename = scanner.nextLine();
        while (!files.contains(filename) && !filename.equals("cancel"))
        {
            System.out.print("Please input a valid file name: ");
            filename = scanner.nextLine();
        }
        return filename.equals("cancel") ? null : filename;
    }

    public static void main(String[] args) throws IOException {
        Socket socket = ByteTransfer.socketInit(args, true);
        new File("Client").mkdir();
        ByteTransfer transfer = new ByteTransfer(new DataInputStream(socket.getInputStream()),
                new DataOutputStream(socket.getOutputStream()));

        transfer.SendBytes(new byte[]{1,0,0,0,0});
        if(transfer.ReceiveBytes()[1] == 2)
            System.out.println("Connection established!");
        String s;
        while (true)
        {
            System.out.println("send | download | ping");
            s = scanner.nextLine();
            switch (s)
            {
                case "exit":
                    transfer.SendBytes(new byte[]{1, 1, 0, 0, 0});
                    if (transfer.ReceiveBytes()[1] == 2)
                        System.out.println("Connection with the server dropped.");
                    socket.close();
                    ByteTransfer.deletefiles("Client/");
                    return;

                case "ping":
                    transfer.SendBytes(new byte[]{1,0,0,0,0});
                    if(transfer.ReceiveBytes()[1] == 2)
                        System.out.println("pong!");
                    break;

                case "send":
                    System.out.println("path of the file");
                    String path = scanner.nextLine();

                    if (!new File(path).exists() && !new File((path)).isDirectory())
                    {
                        System.out.println("File not found! Please restart your request.");
                        break;
                    }

                    transfer.sendFile(path);
                    if (transfer.ReceiveBytes()[1] == 7)
                        System.out.println("Transfer Failed!");
                    else
                        System.out.println("Transfer Done!");
                    break;

                case "download":
                    transfer.SendBytes(new byte[]{1,8,0,0,0});
                    String filename = Client.getFileName(transfer);
                    if (filename == null)
                    {
                        transfer.SendBytes(new byte[]{1,1,0,0,0});
                        break;
                    }
                    transfer.SendBytes(ByteTransfer.toSPOMSP(new byte[]{1,8}, filename.getBytes()));
                    transfer.ReceiveBytes();
                    transfer.ReceiveFile("Client/");
                    if (transfer.ReceiveBytes()[1] == 7)
                        System.out.println("Transfer Failed!");
                    else
                        System.out.println("Transfer Done!");
                    break;

                default:
                    System.out.println("Input not recognized, please try again.");
            }
        }
    }
}
