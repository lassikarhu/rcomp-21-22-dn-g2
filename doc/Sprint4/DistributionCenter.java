import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class DistributionCenter {

    protected static ServerSocket serverSocket;
    protected static final HashMap<FilesHost, ByteTransfer> hosts = new HashMap<>();
    protected static boolean freeze;

    /**
     * Adds host to the list of available Files Host
     *
     * @param socket to add
     * @throws Exception
     */
    protected static synchronized void addhost(Socket socket, ByteTransfer download) throws Exception {
        hosts.put(new FilesHost(socket), download);
    }

    protected static synchronized int numberhosts() {
        return hosts.size();
    }


    /**
     * disconnects socket, identifying if the Client was a Files Host or not.
     *
     * @param socket socket to disconnect
     */
    protected static synchronized void removesocket(Socket socket) {
        try {
            if (!remove(socket))
                System.out.println("A client has disconnected.");
            else
                System.out.println("A File Host has disconnected.");

            socket.close();
        } catch (IOException e) {
        }

    }

    public static boolean remove(Socket socket) throws IOException {
        for (FilesHost fh : hosts.keySet()) {
            if (fh.getSocket() == socket) {
                hosts.get(fh).getSocket().close();
                return hosts.remove(socket, hosts.get(socket));
            }

        }
        return false;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        freeze = false;

        //server init
        try {
            serverSocket = new ServerSocket(9999);
        } catch (IOException ex) {
            System.out.println("Failed to open server socket");
            ex.printStackTrace();
            System.exit(1);
        }

        Socket socket;
        while (true) {
            socket = serverSocket.accept();
            Thread cli = (new Thread(new ServerThread(socket)));
            cli.start();
            TimeUnit.SECONDS.sleep(3);
        }
    }
}

class ServerThread implements Runnable {
    private final Socket socket;
    private ByteTransfer transfer;

    public ServerThread(Socket socket) {
        this.socket = socket;
    }

    /**
     * Elects a Files Host depending on 2 parameters:
     * -Is the file already present?
     * -If not, which FH holds the least amount of files?
     *
     * @param name
     * @return
     */
    private synchronized FilesHost electFH(String name) {
        FilesHost[] fh = DistributionCenter.hosts.keySet().stream().toArray(FilesHost[]::new);

        FilesHost elected = DistributionCenter.hosts.isEmpty() ? null : fh[0];

        if (elected == null)
            return null;

        if (elected.getFiles().contains(name))
            return elected;

        FilesHost temp;
        for (int i = 1; i < fh.length; i++) {
            temp = fh[i];

            if (temp.getFiles().contains(name))
                return temp;

            if (elected.getFileNumber() > temp.getFileNumber())
                elected = temp;
        }
        return elected;
    }

    /**
     * Finds FH holding a certain file
     *
     * @param name of the file
     * @return
     */
    private synchronized FilesHost findFH(String name) {
        for (FilesHost FH : DistributionCenter.hosts.keySet()) {
            for (String file : FH.getFiles()) {
                if (file.equals(name))
                    return FH;
            }
        }
        return null;
    }

    /**
     * Handles file transfer to Files Host
     *
     * @throws IOException
     */
    private synchronized void FileToFH() throws IOException {
        System.out.println("Receiving file...");
        String name = transfer.ReceiveFile("");
        FilesHost elected = electFH(name);

        if (elected == null) {
            System.out.println("No Files Host connected, transfer aborted.");
            transfer.SendBytes(new byte[]{1, 7, 0, 0, 0});
            new File(name).delete();
            return;
        }

        System.out.println("File Received, Sending it to Elected FH");
        elected.getTransfer().sendFile(name);
        elected.addFile(name);
        new File(name).delete();
        transfer.SendBytes(new byte[]{1, 5, 0, 0, 0});
    }

    /**
     * Handles file transfer to Client
     *
     * @throws IOException
     */
    private synchronized void FileToClient() throws IOException {
        SendFileNames();
        byte[] temp = transfer.ReceiveBytes();
        if (temp[1] == 1)
            return;
        String name = new String(Arrays.copyOfRange(temp, 4, temp.length));
        FilesHost FH = findFH(name);
        ByteTransfer FHdl = DistributionCenter.hosts.get(FH);

        System.out.println("Asking for file...");

        if (FH == null) {
            System.out.println("No Files Host with file found, transfer aborted.");
            transfer.SendBytes(new byte[]{1, 7, 0, 0, 0});
            new File(name).delete();
            return;
        }

        FH.getTransfer().SendBytes(ByteTransfer.toSPOMSP(new byte[]{1, 8}, name.getBytes()));

        System.out.println("File Request sent");
        FHdl.ReceiveFile("");
        transfer.sendFile(name);
        transfer.SendBytes(new byte[]{1, 0, 0, 0});
        new File(name).delete();
    }

    /**
     * Sends the name of all files present in every FH.
     *
     * @throws IOException
     */
    private synchronized void SendFileNames() throws IOException {
        for (FilesHost host : DistributionCenter.hosts.keySet()) {
            for (String file : host.getFiles()) {
                transfer.SendBytes(ByteTransfer.toSPOMSP(new byte[]{1, 4}, file.getBytes()));
            }
        }
        transfer.SendBytes(new byte[]{1, 5, 0, 0, 0});
    }

    public void run() {
        try {
            transfer = new ByteTransfer(new DataInputStream(socket.getInputStream()),
                    new DataOutputStream(socket.getOutputStream()));

            while (true) {
                byte[] spomsp = transfer.ReceiveBytes();
                switch (spomsp[1] & 0xFF) {

                    //ping
                    case 0:
                        transfer.SendBytes(new byte[]{1, 2, 0, 0, 0});
                        break;

                    //Client Disconnect
                    case 1:
                        transfer.SendBytes(new byte[]{1, 2, 0, 0, 0});
                        DistributionCenter.removesocket(socket);
                        return;

                    //file host reg
                    case 3:

                        transfer.SendBytes(new byte[]{1, 2, 0, 0, 0});
                        System.out.println("new files host registered!");
                        Socket downloadSys = DistributionCenter.serverSocket.accept();
                        ByteTransfer download = new ByteTransfer(new DataInputStream(downloadSys.getInputStream()),
                                new DataOutputStream(downloadSys.getOutputStream()), downloadSys);
                        System.out.println("Total number of FH: " + (DistributionCenter.numberhosts() + 1));
                        DistributionCenter.addhost(socket, download);
                        break;

                    //Send file
                    case 4:
                        FileToFH();
                        break;

                    //Files Host disconnect
                    case 6:
                        transfer.SendBytes(new byte[]{1, 1, 0, 0, 0});
                        DistributionCenter.removesocket(socket);
                        return;
                    //Client file request
                    case 8:
                        FileToClient();

                    default:
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            DistributionCenter.removesocket(socket);
            return;
        }
    }
}

