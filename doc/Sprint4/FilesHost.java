import java.io.*;
import java.net.*;
import java.util.*;

public class FilesHost {
    protected ByteTransfer t;
    private Socket socket;

    public Socket getSocket() {
        return socket;
    }

    private List<String> files;

    /**
     * Files Host constructor for the DBC
     * @param socket
     * @throws Exception
     */
    public FilesHost(Socket socket) throws Exception {
        this.socket = socket;
        t = new ByteTransfer(new DataInputStream(socket.getInputStream()),
                new DataOutputStream(socket.getOutputStream()));
        this.files = new ArrayList<>();
    }

    public ByteTransfer getTransfer() {
        return t;
    }

    public synchronized void addFile(String name) {
        files.add(name);
    }

    public List<String> getFiles() {
        return files;
    }


    public int getFileNumber() {
        return files.size();
    }





    public static void main(String[] args) {
        try {
            Socket socket = ByteTransfer.socketInit(args);
            new File("Files").mkdir();

            Scanner scanner = new Scanner(System.in);
            ByteTransfer transfer = new ByteTransfer(new DataInputStream(socket.getInputStream()),
                    new DataOutputStream(socket.getOutputStream()));

            transfer.SendBytes(new byte[]{1, 0, 0, 0, 0});
            if (transfer.ReceiveBytes()[1] == 2)
                System.out.println("Connection established!");
            transfer.SendBytes(new byte[]{1, 3, 0, 0, 0});
            if (transfer.ReceiveBytes()[1] == 2)
                System.out.println("Identification success");
            Socket DLsocket = ByteTransfer.socketInit(args);
            ByteTransfer DLtransfer = new ByteTransfer(new DataInputStream(DLsocket.getInputStream()),
                    new DataOutputStream(DLsocket.getOutputStream()));
            System.out.println("Download System online!");
            FilesHostClient thread = new FilesHostClient(transfer, DLtransfer);
            thread.start();
            while (true) {
                String s = scanner.nextLine();
                switch (s)
                {
                    case "exit":
                        transfer.SendBytes(new byte[]{1, 6, 0, 0, 0});
                        ByteTransfer.deletefiles("Files/");
                        socket.close();
                        DLsocket.close();
                        return;
                    case "ping":
                        transfer.SendBytes(new byte[]{1,0,0,0,0});
                        break;
            }
        }
        } catch (IOException e) {
            ByteTransfer.deletefiles("Files/");
            e.printStackTrace();
        }
    }

}

class FilesHostClient extends Thread {
    private ByteTransfer transfer;
    private ByteTransfer DLtransfer;

    public FilesHostClient(ByteTransfer t, ByteTransfer DLt)
    {
        transfer = t;
        DLtransfer = DLt;
    }

    @Override
    public void run() {
        while (true) {
            try {
                byte[] spomsp = transfer.ReceiveBytes();
                switch (spomsp[1] & 0xFF)
                {
                    case 1:
                        System.out.println("Connection with the server dropped.");
                        return;
                    case 2:
                        System.out.println("pong!");
                        break;
                    case 4:
                        System.out.println("Receiving file");
                        transfer.ReceiveFile("Files/");
                        System.out.println("File from DBC received!");
                        break;
                    case 8:
                        String name = new String(Arrays.copyOfRange(spomsp,4,spomsp.length));
                        DLtransfer.sendFile("Files/"+name);
                        break;
                }
            } catch (IOException e) {
                ByteTransfer.deletefiles("Files/");
            }
        }
    }
}


