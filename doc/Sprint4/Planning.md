Planning **Sprint Master**: Théo De Magalhaes, 1212335
===========

# SPOMSP Code Table
More Codes have been implemented to facilitate and clarify Packet Transfer. In total, we are counting 8 different codes, all used for different purposes vital to our system's integrity.
| Code | Meaning                                                                                                                                                                                                         |
|------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0    | **COMM-TEST** - Communications test request with no other effect on the server application than the response with a code two message (ACK). This request has no data.                                           |
| 1    | **DISCONN** - End of session request. The server is supposed to respond with a code two message, afterwards both applications are expected to close the session (TCP connection).  This request has no data.	   |
| 2    | **ACK** - Generic acknowledgment message. Used in response to requests with codes zero and one but may be used for other requests. This response has no data.	                                                  |
| 3    | **FHREG** - Identification request from a Files Host to the server, to which the server responds with *ACK*. This request has no data.                                                                          |
| 4    | **SENDF** - File Transfer request from a Client to a Server instance. Contains DATAd                                                                                           |
| 5    | **ENDF** - File End Transfer request from a Client to Server instance. It contains no data.                                                                                                                     |
| 6    | **FHDISCONN** - End of Files Host request. The server is supposed to respond with a code 1 message, afterwards both applications are expected to close the session (TCP connection).  This request has no data. |
| 7    | **ERROR** Server ping to Client, letting it know a file transfer failed. It has no data.²                                                                                                                       |
 | 8   | **DOWNLOAD** Client ping to server, preparing Server to host a file transfer from a Files Host to a Client.                                                                                                     |



 # Implementation 

  Everything was developed in Java, using the Socket class and the built-in Input/Output Stream classes.  
  The Distribution Center & Files Host have been implemented in multi-threading in order to allow packet traffic to run smoothly.  
  Packet Transfers are handled by a secondary class: ByteTransfer. That way, it will be easier for the Team to implement SPOMSP protected by SSL/TLS.  

 # Task Repartition

**Task 1**: Théo De Magalhaes, 1212335  

**Task 2**: Lassi Karhu, 1212308  

**Task 3**: Alexandru Orac 1212273  

**Task 4**: Anttoni Tapio, 1212343

**Task 5**: Daniel Miguel, 1212349  