import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

public class ByteTransfer {
    private DataInputStream in;
    private DataOutputStream out;
    private Socket socket;

    public ByteTransfer(DataInputStream in, DataOutputStream out) {
        this.in = new DataInputStream(in);
        this.out = new DataOutputStream(out);
    }

    public ByteTransfer(DataInputStream in, DataOutputStream out, Socket socket) {
        this.in = new DataInputStream(in);
        this.out = new DataOutputStream(out);
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    /**
     * Initializes client sockets
     * @param args
     * @return
     */
    public synchronized static Socket socketInit(String[] args) {
        if (args.length != 1) {
            System.out.println(
                    "Server IPv4/IPv6 address or DNS name is required as argument");
            System.exit(1);
        }

        InetAddress serverIP = null;
        try {
            serverIP = InetAddress.getByName(args[0]);
        } catch (UnknownHostException ex) {
            System.out.println("Invalid server: " + args[0]);
            System.exit(1);
        }

        Socket socket = null;
        try {
            socket = new Socket(serverIP, 9999);
        } catch (IOException ex) {
            System.out.println("Failed to connect.");
            System.exit(1);
        }
        return socket;
    }

    /**
     * Handles byte reception
     * @return
     */
    public synchronized byte[] ReceiveBytes() {
        try {
            int len = in.readInt();
            byte[] spomsp;
            spomsp = new byte[len];
            in.read(spomsp, 0, len);
            return spomsp;
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[]{1, 1, 0, 0, 0};
        }
    }

    /**
     * Handles byte transfer
     * @param spomsp
     * @throws IOException
     */
    public synchronized void SendBytes(byte[] spomsp) throws IOException {
        out.writeInt(spomsp.length);
        out.write(spomsp, 0, spomsp.length);
    }

    /**
     * Handles the conversion of data to the SPOMS Protocol
     * @param spomsp Version & Code
     * @param data Data to send
     * @return Byte array complying to the protocol
     */
    //spomsp composed of VER & CODE &
    public static byte[] toSPOMSP(byte[] spomsp, byte[] data) {
        byte[] message = new byte[spomsp.length + 2 + data.length];

        //signed values!! to get unsigned, & 0xFF
        message[0] = spomsp[0];
        message[1] = spomsp[1];

        message[2] = (byte) (data.length % 256);
        message[3] = (byte) (data.length / 256);
        for (int i = 0; i < data.length; i++) {
            message[i + 4] = data[i];
        }
        return message;
    }

    /**
     * Handles Sending a file
     * @param path path of the file
     * @throws IOException if file does not exist (not possible)
     */
    public synchronized void sendFile(String path) throws IOException {
        File file = new File(path);
        long length = file.length();
        FileInputStream fileInputStream;
        fileInputStream = new FileInputStream(file);

        BufferedInputStream fileBuffered = new BufferedInputStream(fileInputStream);

        byte[] name = file.getName().getBytes();
        byte[] spomsp = toSPOMSP(new byte[]{1, 4}, name);
        SendBytes(new byte[]{1, 4});
        SendBytes(spomsp);

        int size = 3996; //3996
        for (long curr = 0; curr != length; size = 3996) {
            if (length - curr >= size)
                curr += size;
            else {
                size = (int) (length - curr);
                curr = length;
            }
            byte[] content = new byte[size];
            fileBuffered.read(content, 0, size);
            SendBytes(toSPOMSP(new byte[]{1, 4}, content));
            System.out.println("Sending file ... " + (curr * 100) / length + "% complete!");
        }
        fileBuffered.close();
        fileInputStream.close();
        SendBytes(new byte[]{1, 5, 0, 0, 0});
    }

    /**
     * Handles file reception
     * @param path path of the output file
     * @return name of the file
     * @throws IOException
     */
    public synchronized String ReceiveFile(String path) throws IOException {

        byte[] temp = ReceiveBytes();

        while (temp.length <= 4)
            temp = ReceiveBytes();

        String name = new String(Arrays.copyOfRange(temp, 4, temp.length));
        FileOutputStream fileOutputStream = new FileOutputStream(path + name);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        byte[] spomsp;
        spomsp = ReceiveBytes();
        while (spomsp[1] != 5) {
            bufferedOutputStream.write(spomsp, 4, spomsp.length - 4);
            spomsp = ReceiveBytes();
        }
        bufferedOutputStream.close();
        fileOutputStream.close();
        return name;
    }

    /**
     * Deletes all files from Files/ upon exiting session
     */
    public synchronized static void deletefiles(String path) {
        File directory = new File(path);
        String[] files = directory.list();
        if (files != null) {
            for (String file : files) {
                if (!new File(path + file).delete()){
                    System.out.println("failed to delete: " + file);
                }
            }
        directory.delete();
        }
    }
}
