\# RCOMP SPRINT 3 - Building 4
------------------------------

Théo De Magalhaes - 1212335
====================

Building 4
==========

Planning
-----------

In this sprint, all the .pkt project definitions from sprint2 were kept to be used as base.

We defined the ids for the OSPF areas, the phone prefixes, the DNS domain names
DNS domain names, the DNS Server name and the respective address of the buildings, in the
planning.md, so for my building

-   Building 4 OSPF area 4

-   Backbone OSPF area 0

-   VOIP num 400

-   VOIP num 401

-   DNS domain name - building-4.rcomp-21-22-dn-g2

-   DNS server name - ns.building-4.rcomp-21-22-dn-g2

-   DNS Server IPs - 172.18.149.240/28

>   As the Project Description requests, one of sprint2's existing servers now
>   hosts all DNS services, so the applied DNS address is 172.18.149.241

PKT File
-----------


### OSPF dynamic routing

- Static routes between buildings have been deleted, except for the default route with connection to the ISP.

- All areas of the different buildings are connected to area 0 (backbone).

- The same steps were done in each router, just changing the internal networks and the number of the area where they will be placed

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      router ospf 1
        log-adjacency-changes
        network 172.18.144.0 0.0.0.127 area 0
        network 172.18.149.0 0.0.0.127 area 4
        network 172.18.149.128 0.0.0.63 area 4
        network 172.18.149.192 0.0.0.31 area 4
        network 172.18.149.224 0.0.0.15 area 4
        network 172.18.149.240 0.0.0.15 area 4
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### HTTP

- A server was placed in the DMZ VLAN to assume the HTTP service

- A HTML page with a Building identifier was added (referring to Building 4)

- That HTML page is only accessible from the 2 Servers

### DHCPv4

- There are 4 VLANS with DHCP configured on the router itself. All but the DMZ.

- The VOIP configuration was done with option 150, as requested in the statement

- The names of the pools used were:
    * wifi
    * firstfloor
    * groundfloor
    * voip

-   All laptops & PCs are now using DHCP.

### VOIP

- On the ports of the switches connected to the telephones the respective voice
    vlan enabled and the access vlan disabled.

- As the DHCP was already configured it is only necessary to configure the
    telephony, where it was defined the max-ephones number, being 2, and the
    phone number of each one:

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ephone-dn 1
      number 400
    
      ephone-dn 2
      number 401
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- The last step was to set up the calls between buildings where the
    dial-peer voice command was used. The pattern chosen to recognize numbers from
    building 1 was 1....., for building 2 it was 2..., for building 3 3..., 
    for building 4 4..., and for building 5... . Each pattern is forwarded
    to the router of the respective building.


### DNS

- The group member with building 1 has created a domain name that will be the
    highest level, so it was used as the DNS domain root

- Records were created as requested in the Project Description:

![dns.png](dns.png)

- DNS was added manually to remaining servers.


### ACLs

  -   Here are the Firewall rules used by Building 4.


  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ip access-list extended wifi
 permit udp 172.18.149.0 0.0.0.127 host 172.18.149.242 eq domain
 permit tcp 172.18.149.0 0.0.0.127 host 172.18.149.242 eq domain
 permit tcp 172.18.149.0 0.0.0.127 host 172.18.149.243 eq www
 permit tcp 172.18.149.0 0.0.0.127 host 172.18.149.243 eq 443
 deny icmp 172.18.149.0 0.0.0.127 host 172.18.149.1
 deny icmp 172.18.149.0 0.0.0.127 172.18.149.240 0.0.0.15
 permit icmp 172.18.149.0 0.0.0.127 any
 permit udp host 0.0.0.0 host 255.255.255.255

ip access-list extended firstfloor
 permit udp 172.18.149.128 0.0.0.63 host 172.18.149.242 eq domain
 permit tcp 172.18.149.128 0.0.0.63 host 172.18.149.242 eq domain
 permit tcp 172.18.149.128 0.0.0.63 host 172.18.149.242 eq www
 permit tcp 172.18.149.128 0.0.0.63 host 172.18.149.242 eq 443
 deny icmp 172.18.149.128 0.0.0.63 host 172.18.149.129
 deny icmp 172.18.149.128 0.0.0.63 172.18.149.240 0.0.0.15
 permit icmp 172.18.149.128 0.0.0.63 any
 permit udp host 0.0.0.0 eq bootpc host 255.255.255.255 eq bootps
 permit tcp 172.18.149.128 0.0.0.63 host 172.18.149.243 eq www
 permit tcp 172.18.149.128 0.0.0.63 host 172.18.149.243 eq 443

ip access-list extended groundfloor
 permit udp 172.18.149.192 0.0.0.31 host 172.18.149.242 eq domain
 permit tcp 172.18.149.192 0.0.0.31 host 172.18.149.242 eq domain
 permit tcp 172.18.149.192 0.0.0.31 host 172.18.149.242 eq www
 permit tcp 172.18.149.192 0.0.0.31 host 172.18.149.242 eq 443
 deny icmp 172.18.149.192 0.0.0.31 host 172.18.149.193
 deny icmp 172.18.149.192 0.0.0.31 172.18.149.240 0.0.0.15
 permit icmp 172.18.149.192 0.0.0.31 any
 permit udp host 0.0.0.0 eq bootpc host 255.255.255.255 eq bootps
 permit tcp 172.18.149.192 0.0.0.31 host 172.18.149.243 eq www
 permit tcp 172.18.149.192 0.0.0.31 host 172.18.149.243 eq 443

ip access-list extended voip
 deny icmp 172.18.149.224 0.0.0.15 172.18.149.128 0.0.0.127
 deny icmp 172.18.149.224 0.0.0.15 host 172.18.149.225
 permit icmp 172.18.149.224 0.0.0.15 any
 permit tcp 172.18.149.224 0.0.0.15 host 172.18.149.225
 permit udp host 0.0.0.0 eq bootpc host 255.255.255.255 eq bootps

ip access-list extended dmz
 permit tcp host 172.18.149.242 eq domain any
 permit udp host 172.18.149.242 eq domain any
 permit tcp host 172.18.149.243 eq www any established
 permit tcp host 172.18.149.243 eq 443 any established
 deny icmp any host 172.18.149.241
 permit icmp any any

ip access-list extended campus
 deny ip 172.18.144.0 0.0.0.127 any
 deny icmp any 172.18.149.128 0.0.0.127
 permit icmp any any
 permit udp any host 172.18.149.242 eq domain
 permit tcp any host 172.18.149.242 eq domain
 permit tcp any host 172.18.149.243 eq www
 permit tcp any host 172.18.149.243 eq 443
 permit ospf 172.18.144.0 0.0.0.127 255.255.255.128 0.0.0.127
 permit tcp 172.18.144.0 0.0.0.127 172.18.144.0 0.0.0.127


  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  and uh that's kinda it? cant wait for this semester to be over fr fr
 

 

 


 

 
