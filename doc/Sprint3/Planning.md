Planning
================

Building 1: 1212308 - Lassi

Building 2: 1212343 - Anttoni (sprint master)

Building 3: 1212273 - Alex

Building 4: 1212335 - Théo

Building 5: 1212349 - Daniel



We'll begin this sprint by removing the static routing from the routing tables and replacing it with OSPF. For that we'll need to establish OSPF area IDs for each building.

##### OSPF Area IDs

Building 1: 1

Building 2: 2

Building 3: 3

Building 4: 4

Building 5: 5

Backbone: 0



Each building must have a new server added, with an HTML page that identifies which building it belongs to. In the naming of the page we will use "buildingx" where the x is replaced with the number of the building.



Each building's router must provide the VoIP service to the local VoIP network that encompasses calls forwarding to other buildings. For that we'll need to know each building VoIP prefixes.

##### VoIP prefixes

Building 1:

- Floor 0: 100101
- Floor 1: 100102

Building 2:

- Floor 0: 2000
- Floor 1: 2001

Building 3:

- Floor 0: 300
- Floor 1: 301

Building 4:

- Floor 0: 400
- Floor 1: 401

Building 5:

- Floor 0: 500
- Floor 1: 501



Each building must have a DNS domain name. For building 1 it will be rcomp-21-22-cc-gn, and for every other building it will be building-x.rcomp-21-22-cc-gn, where the x is replaced with the number of the building. The same logic will be applied in the naming of the DNS name server, just "ns" added on front. The IPv4 node addresses for each building's DNS server will be:

##### DNS server address

Building 1:

- 172.18.144.132

Building 2:

- 172.18.147.227

Building 3:

- 172.18.148.200

Building 4:

- 172.18.149.242

Building 5:

- 172.18.150.227