# Sprint 3

## Building 1

### OSPF

For OSPF I set the backbone network 172.18.144.0 to area 0 for our buildings to be able to communicate. The other networks I set the area to 1 because I had the first building to do.

![image-20220508224509319](C:\Users\lassi\AppData\Roaming\Typora\typora-user-images\image-20220508224509319.png)

### HTTP

HTTP Server address is 172.18.144.131. I modified the index file to show it works in following way:

![image-20220508224747055](C:\Users\lassi\AppData\Roaming\Typora\typora-user-images\image-20220508224747055.png)

### DHCP

I started the DHCP by adding the excluded addresses. The excluded addresses included the gateway addresses of the address ranges for specific DHCP pools because we don't wanna give gateway address to an random endpoint. After that I made DHCP pool for Floor0, Floor1, WiFi and VoIP. For now I only added network and default-router.

![image-20220508224552354](C:\Users\lassi\AppData\Roaming\Typora\typora-user-images\image-20220508224552354.png)

### VoIP

I set the max ephones to 15. First number to 1001 and second to 1002. I got the phones to work.

![image-20220508225116744](C:\Users\lassi\AppData\Roaming\Typora\typora-user-images\image-20220508225116744.png)

### DNS

My DNS address: 172.18.144.132

HTTP Address: 172.18.144.131

The DNS was set up from the servers services.

![image-20220508225958691](C:\Users\lassi\AppData\Roaming\Typora\typora-user-images\image-20220508225958691.png)

The endpoints are using DNS because I set it to DHCP

### NAT

For NAT I only had to redirect the HTTP/HTTPS and DNS traffic in local DMZ

![image-20220508230233468](C:\Users\lassi\AppData\Roaming\Typora\typora-user-images\image-20220508230233468.png)

For backbone network we set the "ip nat outside" but for all the others "ip nat inside".

### ACLs

The ACLs were pretty hard but I think I got them working as needed.

![image-20220508230637526](C:\Users\lassi\AppData\Roaming\Typora\typora-user-images\image-20220508230637526.png)

