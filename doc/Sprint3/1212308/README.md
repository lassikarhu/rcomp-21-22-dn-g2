# Sprint 3

## Building 1

### OSPF

For OSPF I set the backbone network 172.18.144.0 to area 0 for our buildings to be able to communicate. The other networks I set the area to 1 because I had the first building to do.

![ospf](C:\Users\lassi\Pictures\ospf.png)

### HTTP

HTTP Server address is 172.18.144.131. I modified the index file to show it works in following way:

![http](C:\Users\lassi\Pictures\http.png)

### DHCP

I started the DHCP by adding the excluded addresses. The excluded addresses included the gateway addresses of the address ranges for specific DHCP pools because we don't wanna give gateway address to an random endpoint. After that I made DHCP pool for Floor0, Floor1, WiFi and VoIP. For now I only added network and default-router.

![DHCP](C:\Users\lassi\Pictures\DHCP.png)

### VoIP

I set the max ephones to 15. First number to 1001 and second to 1002. I got the phones to work.

![aluuri](C:\Users\lassi\Pictures\aluuri.png)

### DNS

My DNS address: 172.18.144.132

HTTP Address: 172.18.144.131

The DNS was set up from the servers services.

![adns](C:\Users\lassi\Pictures\adns.png)

The endpoints are using DNS because I set it to DHCP

### NAT

For NAT I only had to redirect the HTTP/HTTPS and DNS traffic in local DMZ

![anat](C:\Users\lassi\Pictures\anat.png)

For backbone network we set the "ip nat outside" but for all the others "ip nat inside".

### ACLs

The ACLs were pretty hard but I think I got them working as needed.

![aacl](C:\Users\lassi\Pictures\aacl.png)

