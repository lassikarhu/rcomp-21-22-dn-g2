!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname b2_RT
!
!
!
!
ip dhcp excluded-address 172.18.147.241
ip dhcp excluded-address 172.18.147.129
ip dhcp excluded-address 172.18.147.1
ip dhcp excluded-address 172.18.147.193
!
ip dhcp pool VLANF0
 network 172.18.147.192 255.255.255.224
 default-router 172.18.147.193
 dns-server 172.18.147.227
 domain-name building-2.rcomp-21-22-cc-gn
ip dhcp pool VLANF1
 network 172.18.147.128 255.255.255.192
 default-router 172.18.147.129
 dns-server 172.18.147.227
 domain-name building-2.rcomp-21-22-cc-gn
ip dhcp pool VLANVOIP
 network 172.18.147.240 255.255.255.240
 default-router 172.18.147.241
 option 150 ip 172.18.147.241
ip dhcp pool VLANWIFI
 network 172.18.147.0 255.255.255.128
 default-router 172.18.147.1
 dns-server 172.18.147.227
 domain-name building-2.rcomp-21-22-cc-gn
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017MLKD-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.1
 encapsulation dot1Q 476
 ip address 172.18.147.193 255.255.255.224
 ip access-group gf in
 ip nat inside
!
interface FastEthernet1/0.2
 encapsulation dot1Q 477
 ip address 172.18.147.129 255.255.255.192
 ip access-group ff in
 ip nat outside
!
interface FastEthernet1/0.3
 encapsulation dot1Q 478
 ip address 172.18.147.1 255.255.255.128
 ip access-group wf in
 ip nat outside
!
interface FastEthernet1/0.4
 encapsulation dot1Q 479
 ip address 172.18.147.241 255.255.255.240
 ip access-group voip in
 ip nat outside
!
interface FastEthernet1/0.5
 encapsulation dot1Q 480
 ip address 172.18.147.225 255.255.255.240
 ip access-group dmz in
 ip nat outside
!
interface FastEthernet1/0.6
 encapsulation dot1Q 470
 ip address 172.18.144.2 255.255.255.128
 ip access-group campus in
 ip nat outside
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 1
 log-adjacency-changes
 network 172.18.147.0 0.0.0.127 area 2
 network 172.18.147.128 0.0.0.63 area 2
 network 172.18.147.192 0.0.0.31 area 2
 network 172.18.147.224 0.0.0.15 area 2
 network 172.18.147.240 0.0.0.15 area 2
 network 172.18.144.0 0.0.0.127 area 0
!
ip nat inside source static tcp 172.18.147.226 80 172.18.144.2 80 
ip nat inside source static tcp 172.18.147.226 443 172.18.144.2 443 
ip nat inside source static tcp 172.18.147.227 53 172.18.144.2 53 
ip nat inside source static udp 172.18.147.227 53 172.18.144.2 53 
ip classless
!
ip flow-export version 9
!
!
ip access-list extended wf
 permit udp 172.18.147.0 0.0.0.127 host 172.18.147.227 eq domain
 permit tcp 172.18.147.0 0.0.0.127 host 172.18.147.227 eq domain
 permit tcp 172.18.147.0 0.0.0.127 host 172.18.147.226 eq www
 permit tcp 172.18.147.0 0.0.0.127 host 172.18.147.226 eq 443
 deny icmp 172.18.147.0 0.0.0.127 172.18.147.224 0.0.0.15
 deny icmp 172.18.147.0 0.0.0.127 host 172.18.147.1
 permit icmp 172.18.147.0 0.0.0.127 any
 permit udp host 0.0.0.0 host 255.255.255.255
ip access-list extended voip
 deny icmp 172.18.147.240 0.0.0.15 172.18.147.224 0.0.0.15
 deny icmp 172.18.147.240 0.0.0.15 host 172.18.147.241
 permit icmp 172.18.147.240 0.0.0.15 any
 permit tcp 172.18.147.240 0.0.0.15 host 172.18.147.241
 permit udp host 0.0.0.0 eq bootpc host 255.255.255.255 eq bootps
ip access-list extended gf
 permit udp 172.18.147.192 0.0.0.31 host 172.18.147.227 eq domain
 permit tcp 172.18.147.192 0.0.0.31 host 172.18.147.227 eq domain
 permit tcp 172.18.147.192 0.0.0.31 host 172.18.147.226 eq www
 permit tcp 172.18.147.192 0.0.0.31 host 172.18.147.226 eq 443
 deny icmp 172.18.147.192 0.0.0.31 172.18.147.224 0.0.0.15
 deny icmp 172.18.147.192 0.0.0.31 host 172.18.147.193
 permit icmp 172.18.147.192 0.0.0.31 any
 permit udp host 0.0.0.0 eq bootpc host 255.255.255.255 eq bootps
ip access-list extended ff
 permit udp 172.18.147.128 0.0.0.63 host 172.18.147.227 eq domain
 permit tcp 172.18.147.128 0.0.0.63 host 172.18.147.227 eq domain
 permit tcp 172.18.147.128 0.0.0.63 host 172.18.147.226 eq www
 permit tcp 172.18.147.128 0.0.0.63 host 172.18.147.226 eq 443
 deny icmp 172.18.147.128 0.0.0.63 172.18.147.224 0.0.0.15
 deny icmp 172.18.147.128 0.0.0.63 host 172.18.147.129
 permit icmp 172.18.147.128 0.0.0.63 any
 permit udp host 0.0.0.0 eq bootpc host 255.255.255.255 eq bootps
ip access-list extended dmz
 permit tcp host 172.18.147.226 eq www any established
 permit tcp host 172.18.147.226 eq 443 any established
 permit udp host 172.18.147.227 eq domain any
 permit tcp host 172.18.147.227 eq domain any
 deny icmp any host 172.18.147.225
 permit icmp any any
ip access-list extended campus
 deny ip 172.18.144.0 0.0.0.127 any
 deny icmp any 172.18.147.224 0.0.0.15
 permit icmp any any
 permit udp any host 172.18.147.227 eq domain
 permit tcp any host 172.18.147.227 eq domain
 permit tcp any host 172.18.147.226 eq www
 permit tcp any host 172.18.147.226 eq 443
 permit ospf 172.18.144.0 0.0.0.127 255.255.255.128 0.0.0.127
 permit tcp 172.18.144.0 0.0.0.127 172.18.144.0 0.0.0.127
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 1.....
 session target ipv4:172.18.144.1
!
dial-peer voice 3 voip
 destination-pattern 3..
 session target ipv4:172.18.144.3
!
dial-peer voice 4 voip
 destination-pattern 4..
 session target ipv4:172.18.144.4
!
dial-peer voice 5 voip
 destination-pattern 5..
 session target ipv4:172.18.144.5
!
telephony-service
 max-ephones 35
 max-dn 35
 ip source-address 172.18.147.241 port 2000
 auto assign 1 to 2
!
ephone-dn 1
 number 2000
!
ephone-dn 2
 number 2001
!
ephone 1
 device-security-mode none
 mac-address 000D.BD89.B98E
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0001.63C6.47E8
 type 7960
 button 1:2
!
line con 0
 logging synchronous
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

